﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Distance : MonoBehaviour
{
    public int distance;
    int totalDistance;
    //public Text runDisText;
    public Text distanceText;
    public Text totalDisText;
    int bestDis;
    public Text bestDisText;
    public Controller con;
    public GameState st;
    float time;
    string bestDisPref = "bestDis";
    public Text runScore;
    public Text winScore;
    

    void Start()
    {
        PlayerPrefs.GetInt(bestDisPref, bestDis);
        distance = 0;
    }
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            PlayerPrefs.DeleteAll();
        }
        if (con.RunStarted == true && !con.gameLose)
        {
            time += 1 * Time.deltaTime * 12;
            if (time > 1)
            {
                distance += 1;
                time = 0;
            }

            //if (!st.paused)
            //{
            //    distance++;
            //}


        }


        distanceText.text = "" + distance + "M";
        runScore.text = "" + distance;
        //runDisText.text = "" + distance;
        totalDistance = distance;
        winScore.text = "" + totalDistance + "M";
        if (con.gameLose == true)
        {
           totalDisText.text = "" + totalDistance + "M";
            
            
        }
        if (totalDistance > PlayerPrefs.GetInt(bestDisPref))
        {
            bestDis = totalDistance;
            PlayerPrefs.SetInt(bestDisPref, bestDis);
            
            PlayerPrefs.Save();
        }

        
        bestDisText.text = "" + PlayerPrefs.GetInt(bestDisPref);
    }
}
