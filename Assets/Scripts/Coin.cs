﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{

    public Coins cn;
    CoinMove cm;

    public Transform playerTransform;
    public float moveSpeed = 17f;

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        cm = gameObject.GetComponent<CoinMove>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            cn.coins += 1;
            Destroy(this.gameObject);
            PlayerPrefs.SetInt("coins", cn.coins);
            PlayerPrefs.Save();
        }
        
        if(other.gameObject.tag == "CoinDetector")
        {
            cm.enabled = true;
        }


    }
}
