﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]

public enum SIDE { Left,Mid,Right }



public class Controller : MonoBehaviour
{

    public SIDE m_Side = SIDE.Mid;
    float NewZPos = 0f;
    public bool LeftSwipe;
    public bool RightSwipe;
    public float ZValue;
    private CharacterController m_char;
    private Animator m_Animator;

    public GameState st;

    private float z;
    public float SpeedDodge;
    public bool RunStarted = false;
    public float FwdSpeed;
    public float y = 0.1f;
    public int lvlSpeed;

    public bool gameLose = false;

    GameObject booster;
    GameObject trap;

    public int life;

    CapsuleCollider player;


    public bool corlaunch;


    //test

    public Image red;
    public Image lightb;
    public Image blue;
    public Image green;


    void Start()
    {
        corlaunch = false;
        //test
        red.enabled = false;
        lightb.enabled = false;
        blue.enabled = false;
        green.enabled = false;


        m_char = GetComponent<CharacterController>();
        m_Animator = GetComponent<Animator>();
        m_Animator.SetBool("start", false);
        m_Animator.SetBool("attack", false);
        m_Animator.SetBool("died", false);
        //m_Animator.Play("Wave");
        booster = GameObject.FindGameObjectWithTag("Boost");
        trap = GameObject.FindGameObjectWithTag("Trap");
        life = 0;
        player = GetComponent<CapsuleCollider>();

    }


    // start run
    public void StartRun()
    {

        transform.position = Vector3.zero;
        RunStarted = true;
        Debug.Log(RunStarted);
        lvlSpeed = 1;


    }

    private void OnTriggerEnter(Collider other)
    {
        //way traps
        if (other.transform.tag == "Trap")
        {
            if (life > 0)
            {
                StartCoroutine(Double());


            }
            else
            {
                StartCoroutine(TrapHit());
            }


        }

        IEnumerator Double()
        {
            // работает, сделать анимацию типа спотыкания
            player.enabled = false;
            life = 0;
            yield return new WaitForSeconds(1f);
            player.enabled = true;

        }

        // speed booster
        if (other.gameObject.tag == "Boost")
        {
            StartCoroutine(SpeedBooster());
            Destroy(booster);
        }

        if (other.gameObject.tag == "Double")
        {
            if (life == 0)
            {
                //Debug.Log("lifes" + life);
                life += 1;
            }


        }
    }
    IEnumerator SpeedBooster()
    {
        //Debug.Log("run");
        lvlSpeed = 3;
        yield return new WaitForSeconds(10f);
        Debug.Log("run fast");
        lvlSpeed = 1;
    }

    IEnumerator TrapHit()
    {
        RunStarted = false;
        //m_Animator.Play("die");
        m_Animator.SetBool("died", true); // если без дабл лайф, если С, то другая анимация
        m_char.enabled = false;
        FwdSpeed = 0f;
        gameLose = true;
        yield return new WaitForSeconds(0.6f);
        RunStarted = true;


    }


    void Update()
    {


        if (RunStarted)
        {
            //m_Animator.Play("Running");
            m_Animator.SetBool("start", true); ;
            if (lvlSpeed == 1)
            {
                FwdSpeed = 8f;
            }
            else if (lvlSpeed == 2)
            {
                FwdSpeed = 10f;
            }
            else if (lvlSpeed == 3)
            {
                FwdSpeed = 12f;
            }

            if(corlaunch == false)
            {
                blue.enabled = true;
            }
            else
            {
                blue.enabled = false;
            }

            if (Input.anyKey && corlaunch == false)
            {
                StartCoroutine(Swipe());
            }


            LeftSwipe = Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow);
            RightSwipe = Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow);
            if (LeftSwipe == true)
            {

                green.enabled = true;

                if (m_Side == SIDE.Mid)
                {
                    NewZPos = -ZValue;
                    m_Side = SIDE.Left;
                    //m_Animator.Play("DodgeLeft");
                }
                else if (m_Side == SIDE.Right)
                {
                    NewZPos = 0;
                    m_Side = SIDE.Mid;
                    //m_Animator.Play("DodgeLeft");
                }
            }
            else if (RightSwipe == true)
            {
                if (m_Side == SIDE.Mid)
                {
                    NewZPos = +ZValue;
                    m_Side = SIDE.Right;
                    //m_Animator.Play("DodgeRight");
                }
                else if (m_Side == SIDE.Left)
                {
                    NewZPos = 0;
                    m_Side = SIDE.Mid;
                    //m_Animator.Play("DodgeRight");
                }
            }

            Vector3 moveVector = new Vector3(FwdSpeed * Time.deltaTime, y -= transform.position.y, z - transform.position.z);
            z = Mathf.Lerp(z, NewZPos, Time.deltaTime * SpeedDodge);
            m_char.Move(moveVector);
        }
    }

    IEnumerator Swipe()
    {
        corlaunch = true;

        //красный кубик
        //red.enabled = true;

        Vector2 delta = Input.GetTouch(0).deltaPosition;


            if (delta.x > 30)
            {
                // желтый куибк
                //lightb.enabled = true;

                if (m_Side == SIDE.Mid)
                {
                    NewZPos = -ZValue;
                    m_Side = SIDE.Left;
                    //m_Animator.Play("DodgeLeft");
                }
                else if (m_Side == SIDE.Right)
                {
                    NewZPos = 0;
                    m_Side = SIDE.Mid;
                    //m_Animator.Play("DodgeLeft");
                }

            }
            else if (delta.x < -30) // был 0
            {

                if (m_Side == SIDE.Mid)
                {
                    NewZPos = +ZValue;
                    m_Side = SIDE.Right;
                    //m_Animator.Play("DodgeRight");
                }
                else if (m_Side == SIDE.Left)
                {
                    NewZPos = 0;
                    m_Side = SIDE.Mid;
                }

            }
            yield return new WaitForSeconds(0.01f); //влияет на заддержку перед доджем 0.01f
            corlaunch = false;
       

    }
}
