﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Load : MonoBehaviour
{
    public Slider slide;
    public int slideV;
    public float time;
    public int rand;
    
    void Start()
    {
       

    }

   
    void Update()
    {
        slide.value = slideV;
        rand = Random.Range(4, 9);
        time += 1 * Time.deltaTime;
        if (time >= 1)
        {
            slideV += rand;
            time = 0;
        }
        if (slide.value >= 15)
        {
            SceneManager.LoadScene(1);
        }
    }
}
