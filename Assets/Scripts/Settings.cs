﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    bool musicIs;
    bool soundsIs;

    //sounds img
    public Image snd_on;
    public Image snd_off;

    //music img
    public Image mus_on;
    public Image mus_off;

    void Start()
    {
        PlayerPrefs.SetString("Music", "on");
        PlayerPrefs.SetString("Sounds", "on");
        musicIs = true;
        soundsIs = true;
        snd_off.enabled = false;
        mus_off.enabled = false;
    }

    
    void Update()
    {
        
    }

    public void MusicOn()
    {
        
        if (PlayerPrefs.GetString("Music") == "on")
        {
            musicIs = false;
            mus_on.enabled = false;
            mus_off.enabled = true;
            PlayerPrefs.SetString("Music", "off");
            //audio off
        }
        else
        {
            musicIs = true;
            mus_on.enabled = true;
            mus_off.enabled = false;
            PlayerPrefs.SetString("Music", "on");
            //audio on
        }
    }

    public void SoundsOn()
    {

        if (PlayerPrefs.GetString("Sounds") == "on")
        {
            soundsIs = false;
            snd_on.enabled = false;
            snd_off.enabled = true;
            PlayerPrefs.SetString("Sounds", "off");
            //saunds off
        }
        else
        {
            soundsIs = true;
            snd_on.enabled = true;
            snd_off.enabled = false;
            PlayerPrefs.SetString("Sounds", "on");
            //sounds on
        }
    }

    public void GoogleMarket()
    {
        Application.OpenURL("https://play.google.com/");
    }
}
