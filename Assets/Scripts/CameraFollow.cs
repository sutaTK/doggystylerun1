﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform player;
    public Transform camera;
    private Vector3 offset;
    private Vector3 startPos;
    private Vector3 runPos;
    public float y;
    public float speedFollow = 3f;
    public bool camOnPos;

    public Controller con;
    public Animator anim;


    //Камера готова, если что вносить корректировки по скорости анимации и по координатам

    void Start()
    {
        anim = GetComponent<Animator>();
        camOnPos = false;
        //offset = transform.position;
        //startPos = new Vector3(-3, 1, -0.06f);
        runPos = new Vector3(-3.3f, 2.3f, 0);
        camera.transform.position = new Vector3(-3, 1, -0.06f);

    }

    IEnumerator CameraPos()
    {
        anim.Play("CameraRotate");
        camera.transform.position = runPos;
        yield return new WaitForSeconds(0.3f); // 0.3 на поворот камеры анимацией
        camOnPos = true;
        Debug.Log(camOnPos);
    }
   
    void LateUpdate()
    {
        if (con.RunStarted)
        {
            StartCoroutine(CameraPos());

            if(camOnPos == true)
            {
                Debug.Log(runPos);
                Vector3 followPos = player.position + runPos;
                
                RaycastHit hit;
                if (Physics.Raycast(player.position, Vector3.down, out hit, 2.5f))
                    y = Mathf.Lerp(y, hit.point.y, Time.deltaTime * speedFollow);
                else y = Mathf.Lerp(y, player.position.y, Time.deltaTime * speedFollow);
                followPos.y = runPos.y + y;
                transform.position = followPos;
                //Debug.Log(followPos);
            }


        }

    }

  
}
