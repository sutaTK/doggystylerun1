﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PathGenerator : MonoBehaviour
{
    
    public Controller con;
    public Distance di;
    
    //levels
    public GameObject[] path;
    public GameObject[] PpathW;
    public GameObject[] pathW;
    public GameObject[] PpathA;
    public GameObject[] pathA;
    public GameObject[] finalP;
    public GameObject randPath;
    public int gameLvl;
    int i;

    //endless mode
    public Image endl_on;
    public bool endless;

    //finish
    public GameObject WinPanel;
    public bool finished;

    private void Start()
    {
        WinPanel.SetActive(false);
        PlayerPrefs.SetString("Endless", "off");
        endless = false;
        endl_on.enabled = false;
        i = 1;
        gameLvl = i;
    }

    void Update()
    {
        
        
        GameObject[] pathToDestroy = GameObject.FindGameObjectsWithTag("Path");
        if (pathToDestroy.Length >= 4)
        {
            Destroy(pathToDestroy[0]);
        }

        // коллайдеры каждый (340м +-), для переходного нужно ставить на следующий коллайдер, что бы его несколько раз не прогрузило
        if (di.distance < 500) //3600
        {
            //Debug.Log("начало");
            gameLvl = 1;
        }
        if (di.distance < 550 && di.distance > 500) //3900 & 3600
        {
            Debug.Log("переход на зиму");
            gameLvl = 2;
        }
        if (di.distance < 1200 && di.distance > 550) //7500 & 3900
        {
            Debug.Log("зима");
            gameLvl = 3;
            con.lvlSpeed = 2;
        }
        if (di.distance < 350 && di.distance > 300) //7500 & 3900
        {
            Debug.Log("переход на лето");
            gameLvl = 4;
        }
        if (di.distance < 600 && di.distance > 500) //11400 & 7800
        {
            Debug.Log("лето");
            gameLvl = 5;
            con.lvlSpeed = 3;
            if (endless == true)
            {
                if(di.distance > 550)
                {
                    Debug.Log("концовка");
                    gameLvl = 6;
                }
            }
        }
        if(di.distance < 16900 && di.distance > 11400) // 16900 & 11400
        {
            gameLvl = 1;
        }
        if(di.distance < 17200 && di.distance > 16900) // 17200 & 16900
        {
            gameLvl = 2;
        }
        if (di.distance < 21700 && di.distance > 17200) // 21700 & 17200
        {
            gameLvl = 3;
        }
        if (di.distance < 22000 && di.distance > 21700) // 22000 & 21700
        {
            gameLvl = 4;
        }
        if (di.distance < 70000 && di.distance > 22000) // 70000 & 22000
        {
            gameLvl = 5;
        }

    }


    void Generator(Vector3 centre, float radius)
    {
        // нужно рандом ставить на 1 больше, по логике у меня 5 префабов начиная от 0, должно
        // быть (0, 4) но последний не появлялься в рандоме пока не поставил (0, 5)

        
        int PathIndex = Random.Range(0, 5);
        if (gameLvl == 1)
        {
            randPath = path[PathIndex];
        }
        else if (gameLvl == 2)
        {
            randPath = PpathW[PathIndex];
        }
        if(gameLvl == 3)
        {
            randPath = pathW[PathIndex];
        }
        else if(gameLvl == 4)
        {
            randPath = PpathA[PathIndex];
        }
        if (gameLvl == 5)
        {
            randPath = pathA[PathIndex];
        }
        else if (gameLvl == 6)
        {
            randPath = finalP[PathIndex];
        }
        

        if(di.distance > 630)
        {
            StartCoroutine(Finish());
        }
        
        
       
        Collider[] colliders = Physics.OverlapSphere(centre, radius);
        Instantiate(randPath, centre, Quaternion.identity);
        transform.position = new Vector3(transform.position.x + 68f, transform.position.y, transform.position.z);
        /*if(colliders.Length == 0)
        {
            Instantiate(randPath, centre, Quaternion.identity);
            transform.position = new Vector3(transform.position.x + 35, transform.position.y, transform.position.z);
        }*/
    }
    // выставлял нужные координаты, все подвязано я удлинил платформы до 70
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            Debug.Log("collider" + di.distance);
            Generator(new Vector3(transform.position.x + 104f -0.3f, 0), 1.5f);
        }
    }
    

    IEnumerator Finish()
    {
        finished = true;
        WinPanel.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        Time.timeScale = 0;
    }

    public void EndlessMode()
    {

        if (PlayerPrefs.GetString("Endless") == "off")
        {
            endl_on.enabled = true;
            endless = true;
            PlayerPrefs.SetString("Endless", "on");
            //ebless on
        }
        else
        {
            endl_on.enabled = false;
            endless = false;
            PlayerPrefs.SetString("Endless", "off");
            //ebless off
        }
    }
}
