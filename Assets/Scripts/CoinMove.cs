﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinMove : MonoBehaviour
{

    Coin cn;
    // Start is called before the first frame update
    void Start()
    {
        cn = gameObject.GetComponent<Coin>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, cn.playerTransform.position, cn.moveSpeed * Time.deltaTime);
    }
}
